# Usermanager for Cotonti siena #

This plugin allows Cotonti-powered website administrator to edit, create and grant temporary group access to users accounts from the admin panel. Usermanager is compatible with the [Yukon Admin Panel](https://github.com/seditio/yukon)

### What is Usermanager used for? ###

* View user account list with filtering by country & main group
* Search user accounts by name or e-mail
* Edit user accounts
* Delete user accounts
* Quickly add user accounts
* Edit own profile
* Grant temporary group access for a user account

### Setup and config ###

* Unpack files to the plugins/userman folder
* Upload via FTP or repository
* Install the plugin from the admin panel
* Use $R['Usermanager'] = 'userman'; in the yukon.php file to use the plugin from the Yukon admin theme

The configuration values for the plugin are basically the default values for the new account form.

### Versions ###

* 1.0 Initial release